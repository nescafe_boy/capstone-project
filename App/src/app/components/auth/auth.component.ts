import { Component, OnInit, Renderer2 } from '@angular/core';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserModel } from 'src/app/model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private _currentUserService: CurrentUserService, private _renderer: Renderer2) { }

  ngOnInit() {
    this._fetchCurrentUser();
  }

  private _fetchCurrentUser() {
    this._currentUserService.fetch().toPromise().then((user: UserModel) => {
      this._currentUserService.setUser(user);
    });
  }

  ngAfterViewInit(){
    this._renderer.addClass(document.body,'nav-md');
  }

  ngOnDestroy(){
    this._renderer.removeClass(document.body,'nav-md');
  }

}
