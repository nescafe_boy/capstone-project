import { BehaviorSubject, Observable } from 'rxjs';
import { EnrollStudentModel } from '../model';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';


@Injectable({
  providedIn: 'root'
})
export class EnrolledstudentService {
  private _students = new BehaviorSubject<EnrollStudentModel[]>([]);

  constructor( private _http:HttpService) { }

  fetch(){
    return this._http.getRequest('/api/enrollStudent');
  }

  sortResult(data:any){
    return this._http.getRequest('/api/enrollStudent/'+data);
  }

  set setStudent(student: EnrollStudentModel[]) {
    this._students.next(student);
  }

  get getStudent(): Observable<EnrollStudentModel[]> {
    return this._students.asObservable();
  }
}
