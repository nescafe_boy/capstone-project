<?php

use Illuminate\Database\Seeder;

use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name'        => 'Lesther',
            'middle_name'       => 'Andrade',
            'last_name'         => 'Bualan',
            'user_type'			=> 'ADMIN',
            'id_number'         => '0000000',
            'email'             => 'admin@testing.com',
            'password'          => Hash::make('admin'),
        ]);
    }
}
