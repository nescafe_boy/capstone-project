import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessNavComponent } from './guess-nav.component';

describe('GuessNavComponent', () => {
  let component: GuessNavComponent;
  let fixture: ComponentFixture<GuessNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuessNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
