import { Component, OnInit } from '@angular/core';
import { registerStudentModel } from 'src/app/model';
import { StudentconfirmService } from 'src/app/services/studentconfirm.service';
import { StorestudentService } from 'src/app/services/storestudent.service';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { EnrollstudentService } from 'src/app/services/enrollstudent.service';
import { RegisterService } from 'src/app/services/register.service';




@Component({
  selector: 'app-studentlist',
  templateUrl: './studentlist.component.html',
  styleUrls: ['./studentlist.component.css']
})
export class StudentlistComponent implements OnInit {
 
  students: registerStudentModel[];
  selectedStudents: FormGroup;
  enrollStudentData: FormGroup;
  reference_code:any;

  referenceCodeForm: FormGroup;
  constructor(
    private _StudentConfirm : StudentconfirmService, 
    private _storeStudent: StorestudentService,
    private _enrollStudent:EnrollstudentService,
    private _fb: FormBuilder,
    private _register: RegisterService,
    ){ }

  ngOnInit() {
	  this.enrollStudentData = this._fb.group({
      lrn_input: '',
      studfname_input: '',
      studlname_input: '',
      studmname_input: '',
      studdate_input: '',
      studgender_input: '',
      studcity_input: '',
      studzip_input: '',
      pfatherfn_input: '',
      pfathermn_input: '',
      pfatherln_input: '',
      pmotherfn_input: '',
      pmothermn_input: '',
      pmotherln_input: '',
      contact: '',
      ogsa_sc: '',
      ogsa_pr: '',
      otva_sc: '',
      otva_pr: '',
      oat_sc: '',
      oat_pr: '',
      average: '',
	  });
	  this.referenceCodeForm = this._fb.group({
	  	code: ''
	  });
    this._fetchStudents();
    this._watchStudents();
  }

  private _fetchStudents(){
    this._StudentConfirm.fetch().toPromise().then((students: registerStudentModel[]) => {
      this._storeStudent.setStudent = students;
    });
  }

  private _watchStudents(){
    this._storeStudent.getStudent.subscribe((students : registerStudentModel[])=>{
      this.students = students;
    });
  }

  private viewStudent(student){
    this.selectedStudents = student;
  }

  private buildData(student){
    this.selectedStudents = student;
  }

  private acceptStudent(){

    this.reference_code = this.referenceCodeForm.get('code').value;
    const selectedStudent:any = this.selectedStudents;

    if (this.reference_code == selectedStudent.ref_code) {
      console.log("Match!!");
     try {
      this._enrollStudent.create(selectedStudent).toPromise().then((response)=>{
        console.log(response);
        this.deleteStudent(selectedStudent.id);
      });
     } catch (error) {
       
     }
    }else{
      alert("Mismatch!!");
    }
  }
  private deleteStudent(id){
    
    this._register.destroy(id).toPromise().then((resp)=>{
      console.log(resp);
      this._fetchStudents();
      this._watchStudents();
    }).catch((error)=>{
      console.log(error);
    });
    
  }
}
