import { TestBed } from '@angular/core/testing';

import { ViewStudentInfoService } from './view-student-info.service';

describe('ViewStudentInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ViewStudentInfoService = TestBed.get(ViewStudentInfoService);
    expect(service).toBeTruthy();
  });
});
