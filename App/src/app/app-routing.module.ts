import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnrollmentPageComponent } from './components/enrollment-page/enrollment-page.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { ClassifyComponent } from './components/classify/classify.component';
import { AddTeacherComponent } from './components/add-teacher/add-teacher.component';
import { AuthComponent } from './components/auth/auth.component';
import { StudentlistComponent } from './components/studentlist/studentlist.component';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { EnrolledStudentComponent } from './components/enrolled-student/enrolled-student.component';
import { CreateAdminAccountComponent } from './components/create-admin-account/create-admin-account.component';
import { ViewStudentsInfoComponent } from './components/view-students-info/view-students-info.component';
import { FillupFormComponent } from './components/fillup-form/fillup-form.component';


const routes: Routes = [
  { path:'ask-classify', component: ClassifyComponent},
  { path:'register-student', component:EnrollmentPageComponent },
  { path:'fillup', component:FillupFormComponent },
  { path:'view-information', component:ViewStudentsInfoComponent },
  { path:'login', component: LoginFormComponent },
  { path:'auth', component: AuthComponent,
    children: [
      { path:'teacher/add',component: AddTeacherComponent },
      { path: 'list/pending',component: StudentlistComponent },
      { path: '', component: AdminHomeComponent},
      { path: 'list/enrolled', component: EnrolledStudentComponent},
      { path: 'create-account', component: CreateAdminAccountComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
