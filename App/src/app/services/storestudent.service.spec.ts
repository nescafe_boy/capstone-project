import { TestBed } from '@angular/core/testing';

import { StorestudentService } from './storestudent.service';

describe('StorestudentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorestudentService = TestBed.get(StorestudentService);
    expect(service).toBeTruthy();
  });
});
