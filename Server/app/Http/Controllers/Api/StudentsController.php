<?php   

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Student;

class StudentsController extends Controller
{
    public function index()
    {
        return Student::get();
    }

    public function destroy($id){
        $status = Student::find($id)->delete();
        return [$status];
    }
}
