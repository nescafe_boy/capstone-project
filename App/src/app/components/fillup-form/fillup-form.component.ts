import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { StorestudentService } from 'src/app/services/storestudent.service';
import { registerStudentModel } from 'src/app/model';
import { StudentconfirmService } from 'src/app/services/studentconfirm.service';
import { StudentlistComponent } from '../studentlist/studentlist.component';
import { ViewStudentInfoService } from 'src/app/services/view-student-info.service';
@Component({
  selector: 'app-fillup-form',
  templateUrl: './fillup-form.component.html',
  styleUrls: ['./fillup-form.component.css']
})
export class FillupFormComponent implements OnInit {
  form: FormGroup;
  students: registerStudentModel[];
  constructor( 
    private _fb: FormBuilder,
    private _storeStudent: StorestudentService,
    private _StudentConfirm : StudentconfirmService,
    private _viewstudentinfo: ViewStudentInfoService
    ) { }

  ngOnInit() {
    this.form = this._fb.group({
      lrn_number: ['',[Validators.required]],
      firstname: ['',[Validators.required]],
      lastname: ['',[Validators.required]],
      birthdate: ['',[Validators.required]]
    });
    this._fetchStudents();
   this._watchStudents();
  }


  private _watchStudents(){
    this._storeStudent.getStudent.subscribe((students : registerStudentModel[])=>{
      this.students = students;
    });
  }

  private _fetchStudents(){
    this._StudentConfirm.fetch().toPromise().then((students: registerStudentModel[]) => {
      this._storeStudent.setStudent = students;
    });
  }

  submit(){
    try {
      this.decision();
      //console.log(this.students);
    } catch (error) {
      console.log(error);
    }
   
   //console.log("hello");
  }

  private decision(){
    const form = {...this.form.value}

    for (let index = 0; index < this.students.length; index++) {
      if (form['lrn_number']==this.students[index].lrn_input && form['firstname']==this.students[index].studfname_input && form['lastname']==this.students[index].studlname_input && form['birthdate']==this.students[index].studdate_input) {
        console.log("match!!");
        //this._viewstudentinfo.setID(index);
        localStorage.setItem('id',JSON.stringify(this.students[index].id));
        window.location.href = "/view-information";
      }else{
        console.log("missmatch!!");
      }
    }

  }

}
