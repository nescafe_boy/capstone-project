import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private _headers: any;

  constructor(private _http: HttpClient, private _auth: AuthService) {
    const auth = this._auth.auth;
    //console.log({"Accept": "Application/json", "Authorization": `${auth.token_type} ${auth.access_token}`});
    if (!auth){
      return;
    }
    this._headers = new HttpHeaders({"Accept": "Application/json", "Authorization": `${auth.token_type} ${auth.access_token}`});
  }

  postRequest(url, data = {}) {
    return this._http.post(environment.serverUrl + url, data, { headers: this._headers });
  }

  getRequest(url, data = {}) {
    return this._http.get(environment.serverUrl + url, { headers: this._headers, params: data });
  }

  deleteRequest(url, data = {}) {
    return this._http.delete(environment.serverUrl + url, { headers: this._headers, params: data });
  }

  updateRequest(url, data = {}) {
    return this._http.put(environment.serverUrl + url, data, { headers: this._headers });
  }
}
