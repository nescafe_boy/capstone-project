import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ViewStudentInfoService } from 'src/app/services/view-student-info.service';
import { StudentconfirmService } from 'src/app/services/studentconfirm.service';
import { StorestudentService } from 'src/app/services/storestudent.service';
import { registerStudentModel } from 'src/app/model';


@Component({
  selector: 'app-view-students-info',
  templateUrl: './view-students-info.component.html',
  styleUrls: ['./view-students-info.component.css']
})
export class ViewStudentsInfoComponent implements OnInit {
  form : FormGroup;
  id:any;
  students: registerStudentModel[];
  records : FormGroup;

  constructor(
    private _viewstudentinfo: ViewStudentInfoService,
    private _fb:FormBuilder,
    private _StudentConfirm : StudentconfirmService, 
    private _storeStudent: StorestudentService
  ) { }

  ngOnInit() {
  //  this.id = this._viewstudentinfo.id;
    this.form = this._fb.group({
      lrn_input: ['',[Validators.required]],
      studlname_input: ['',[Validators.required]],
      studfname_input: ['',[Validators.required]],
      studmname_input: ['',[Validators.required]],
      studdate_input: ['',[Validators.required]],
      studgender_input: ['Male', [Validators.required]],
      studcity_input: ['',[Validators.required]],
      studzip_input: ['',[Validators.required]],
      pfatherfn_input: ['',[Validators.required]],
      pfathermn_input: ['',[Validators.required]],
      pfatherln_input: ['',[Validators.required]],
      pmotherfn_input: ['',[Validators.required]],
      pmothermn_input: ['',[Validators.required]],
      pmotherln_input: ['',[Validators.required]],
      contact: ['',[Validators.required]],
      strand: ['',[Validators.required]],
    });
    this.id = this._viewstudentinfo.getID()
    this._fetchStudents();
    this._watchStudents();
  }
  private _fetchStudents(){
    this._StudentConfirm.fetch().toPromise().then((students: registerStudentModel[]) => {
      this._storeStudent.setStudent = students;
    });
  }

  private _watchStudents(){
    this._storeStudent.getStudent.subscribe((students : registerStudentModel[])=>{
      this.students = students;
    });
  }
  test(){
    console.log(this.students);
    for (let index = 0; index < this.students.length; index++) {
      if (this.id == this.students[index].id) {
        
      }
    }
  }
}
