import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { UserModel } from '../model';
@Injectable({
  providedIn: 'root'
})
export class RegisterAdminService {

  constructor(
    private _http : HttpService
  ) { }

  create(registeradmin : UserModel){
    return this._http.postRequest('/api/registeradmin',registeradmin);
    //return registeradmin;
  }
}
