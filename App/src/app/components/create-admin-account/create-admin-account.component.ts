import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { RegisterAdminService } from 'src/app/services/register-admin.service';

@Component({
  selector: 'app-create-admin-account',
  templateUrl: './create-admin-account.component.html',
  styleUrls: ['./create-admin-account.component.css']
})
export class CreateAdminAccountComponent implements OnInit {
  form: FormGroup;
  constructor(
    private _fb: FormBuilder,
    private _registeradmin: RegisterAdminService
  ) { }

  ngOnInit() {
    this.form = this._fb.group({
      firstname: ['',[Validators.required]],
      lastname: ['', [Validators.required]],
      middlename: ['', [Validators.required]],
      idnumber: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['',[Validators.required]]
    });
  }

  submit(){
    const adminformvalue = {...this.form.value}
    this._registeradmin.create(adminformvalue).toPromise().then((resp)=>{
      console.log(resp);
    });

  }
}