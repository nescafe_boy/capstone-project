import { Component, OnInit,  Renderer2, HostListener} from '@angular/core';

@Component({
  selector: 'app-guess-nav',
  templateUrl: './guess-nav.component.html',
  styleUrls: ['./guess-nav.component.css'],
  
})
export class GuessNavComponent implements OnInit {
  
  constructor(private _renderer: Renderer2) { }

  ngOnInit() {

  }

  @HostListener("window:scroll", [])
    onWindowScroll() {
       if (window.pageYOffset > 2){
      this._renderer.addClass(document.querySelectorAll('.guessNav')[0],'navOpacity');
    }else{
      this._renderer.removeClass(document.querySelectorAll('.guessNav')[0],'navOpacity');
    }
  }
}
