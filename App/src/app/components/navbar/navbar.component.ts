import { Component, OnInit, OnDestroy } from '@angular/core';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserModel } from 'src/app/model';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  currentUser: UserModel;

  private _currentUserSubscription: Subscription;

  constructor(private _currentUserService: CurrentUserService, private _authService: AuthService) { }

  ngOnInit() {
    this._currentUserSubscription = this._currentUserService.getUser().subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnDestroy() {
    this._currentUserSubscription.unsubscribe();
  }

  logout() {
    this._authService.logout();
  }
}
