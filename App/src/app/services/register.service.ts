import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { registerStudentModel } from '../model';

@Injectable({
  providedIn: 'root'
})

export class RegisterService {

  constructor(private _http: HttpService) { }

  create(preStudent: registerStudentModel){
    return this._http.postRequest('/api/register', preStudent);
  }
  destroy(id :any){
    return this._http.deleteRequest('/api/getStudent/'+id);
    //return console.log(id);
  }
}
