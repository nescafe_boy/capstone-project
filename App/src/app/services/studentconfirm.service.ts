import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class StudentconfirmService {

  constructor(private _http:HttpService) { }

  fetch() {
    return this._http.getRequest('/api/getStudent');
  }
  
}
