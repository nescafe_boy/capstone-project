<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class RegisterAdminController extends Controller
{
    public function store(Request $request){

        return User::create([
            'first_name'    => $request->firstname,
            'middle_name'   => $request->middlename,
            'last_name'     => $request->lastname,
            'user_type'     => 'ADMIN',
            'id_number'     => $request->idnumber,
            'email'         => $request->email,
            'password'      => \Hash::make($request->password)
        ]);
        
    }   
}
