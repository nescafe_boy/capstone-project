import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStudentsInfoComponent } from './view-students-info.component';

describe('ViewStudentsInfoComponent', () => {
  let component: ViewStudentsInfoComponent;
  let fixture: ComponentFixture<ViewStudentsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStudentsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStudentsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
