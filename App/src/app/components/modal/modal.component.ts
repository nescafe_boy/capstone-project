import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

    @Input('code') code;
    @Input('isOpen') isOpen;
    @Input('open') open;

  constructor() { }

  ngOnInit() {
  }

    close() {
        this.open(false);
        window.location.href = "http://localhost/thesis/landing-page";
    }

}
