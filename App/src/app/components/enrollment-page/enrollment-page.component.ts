import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { registerStudentModel } from 'src/app/model';
import { RegisterService } from 'src/app/services/register.service';
import { ClassifierService } from 'src/app/services/classifier.service';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { analyzeAndValidateNgModules } from '@angular/compiler';


@Component({
  selector: 'app-enrollment-page',
  templateUrl: './enrollment-page.component.html',
  styleUrls: ['./enrollment-page.component.css']
})
export class EnrollmentPageComponent implements OnInit {
  form:FormGroup;
  clasifyForm: FormGroup;
    isOpen: boolean = false;
    code: string = "GENERATING CODE";

  constructor(
    private _fb: FormBuilder,
    private _register: RegisterService,
    private _classify: ClassifierService
    ) { }

  ngOnInit() {
    this._setupForm();
    this._classifyData();
  }

  submit(){
    if(!(this.form.get('studgender_input').value == '-- Select Gender --')){
      if(this.form.valid){
        const infoGroup = {...this.form.value};
        this._register.create(infoGroup).toPromise().then((resp:any)=>{
          this.code = resp.ref_code; 
          this.openFunction(true);
          this.form.reset();
        }).catch((error)=>{
          console.log(error);
        }); 
        this._classify.pass(infoGroup).toPromise().then((resp)=>{
          console.log(resp);
        });
        //this._classify.pass(infoGroup);
      }else{
          alert('Form in not valid!');
      }
    }else{
        alert("Invalid Selection of Gender");
    }
    
  }

    openFunction(opt: boolean) {
        this.isOpen = opt;
    }

  private _setupForm(){
    this.form = this._fb.group({
      lrn_input: ['',[Validators.required, Validators.maxLength(255)]],
      studlname_input: ['',[Validators.required, Validators.maxLength(255)]],
      studfname_input: ['',[Validators.required, Validators.maxLength(255)]],
      studmname_input: ['',[Validators.required, Validators.maxLength(255)]],
      studdate_input: ['',[Validators.required, Validators.maxLength(255)]],
      studgender_input: ['Male', [Validators.required]],
      studcity_input: ['',[Validators.required, Validators.maxLength(255)]],
      studzip_input: ['',[Validators.required, Validators.maxLength(255)]],
      pfatherfn_input: ['',[Validators.required, Validators.maxLength(255)]],
      pfathermn_input: ['',[Validators.required, Validators.maxLength(255)]],
      pfatherln_input: ['',[Validators.required, Validators.maxLength(255)]],
      pmotherfn_input: ['',[Validators.required, Validators.maxLength(255)]],
      pmothermn_input: ['',[Validators.required, Validators.maxLength(255)]],
      pmotherln_input: ['',[Validators.required, Validators.maxLength(255)]],
      contact: ['',[Validators.required, Validators.maxLength(255)]],
      ogsa_sc: ['',[Validators.required]],
      ogsa_pr: ['',[Validators.required]],
      otva_sc: ['',[Validators.required]],
      otva_pr: ['',[Validators.required]],
      oat_sc: ['',[Validators.required]],
      oat_pr: ['',[Validators.required]],
      average: ['',[Validators.required]]
    });
  }

  private _classifyData(){
    this.clasifyForm = this._fb.group({
      ogsa_sc: ['',[Validators.required]],
      ogsa_pr: ['',[Validators.required]],
      otva_sc: ['',[Validators.required]],
      otva_pr: ['',[Validators.required]],
      oat_sc: ['',[Validators.required]],
      oat_pr: ['',[Validators.required]],
      average: ['',[Validators.required]]
    });
  }

  resetForm(){
    this.form.reset();
  }

}
