import { TestBed } from '@angular/core/testing';

import { EnrollstudentService } from './enrollstudent.service';

describe('EnrollstudentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnrollstudentService = TestBed.get(EnrollstudentService);
    expect(service).toBeTruthy();
  });
});
