import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { registerStudentModel } from '../model';

@Injectable({
  providedIn: 'root'
})
export class StorestudentService {
  private _students = new BehaviorSubject<registerStudentModel[]>([]);

  constructor() { }

    set setStudent(student: registerStudentModel[]) {
        this._students.next(student);
    }

    get getStudent(): Observable<registerStudentModel[]> {
        return this._students.asObservable();
    }
}
