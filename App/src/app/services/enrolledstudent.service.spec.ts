import { TestBed } from '@angular/core/testing';

import { EnrolledstudentService } from './enrolledstudent.service';

describe('EnrolledstudentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnrolledstudentService = TestBed.get(EnrolledstudentService);
    expect(service).toBeTruthy();
  });
});
