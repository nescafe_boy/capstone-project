import { Component, OnInit, AfterViewInit, Renderer2, OnDestroy } from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit, AfterViewInit,OnDestroy {

  form: FormGroup;  

  constructor(private _fb: FormBuilder, private _auth: AuthService, private _renderer: Renderer2) { }

  ngOnInit() {
    this.form = this._fb.group({
      username:['',[Validators.required]],
      password:['',[Validators.required]]
    });
  }

  ngAfterViewInit() {
    this._renderer.addClass(document.body,'login');
  }

  ngOnDestroy(){
    this._renderer.removeClass(document.body,'login');
  }

  login(){
    this._auth.login(this.form.value);
    //console.log(this.form.value)
  }
}
