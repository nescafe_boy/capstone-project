export interface registerStudentModel{
    id: number;
    lrn_input: String; 
    studlname_input: String; 
    studfname_input: String; 
    studmname_input: String; 
    studdate_input: String; 
    studgender_input: String; 
    studcity_input: String; 
    studzip_input: String; 
    pfatherfn_input: String; 
    pfathermn_input: String; 
    pfatherln_input: String; 
    pmotherfn_input: String; 
    pmothermn_input: String; 
    pmotherln_input: String; 
    contact: String;
    ogsa_sc: number;
    ogsa_pr: number;
    otva_sc: number;
    otva_pr: number;
    oat_sc: number;
    oat_pr: number;
    average: number;
}

export interface EnrollStudentModel{
    id: number;
    lrn_input: String; 
    studlname_input: String; 
    studfname_input: String; 
    studmname_input: String; 
    studdate_input: String; 
    studgender_input: String; 
    studcity_input: String; 
    studzip_input: String; 
    pfatherfn_input: String; 
    pfathermn_input: String; 
    pfatherln_input: String; 
    pmotherfn_input: String; 
    pmothermn_input: String; 
    pmotherln_input: String; 
    contact: String;
    ogsa_sc: number;
    ogsa_pr: number;
    otva_sc: number;
    otva_pr: number;
    oat_sc: number;
    oat_pr: number;
    average: number;
}
export interface AccessTokenModel {
    access_token: string;
    expires_in: string;
    refresh_token: string;
    token_type: string;
}

export interface GenerateAccessTokenModel {
    grant_type: string,
    client_id: number,
    client_secret: string,
    username: string,
    password: string,
    scope: string;
}

export interface LoginModel {
    username: string;
    password: string;
}

export interface UserModel {
    id: number;
    email: string;
    id_number: number;
    first_name: string;
    middle_name: string;
    last_name: string;
    user_type: string;
    updated_at: string;
    created_at: string;
}
export interface ClassifyData{
    ogsa_sc: number;
    ogsa_pr: number;
    otva_sc: number;
    otva_pr: number;
    oat_sc: number;
    oat_pr: number;
    average: number;
}