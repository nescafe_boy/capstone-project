<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>

<?php
    $app_url = 'http://localhost:4200';
?>

<html lang="en">
<head>
<title>website</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Education Hub Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
<!-- //css files -->
<!-- online-fonts -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&subset=latin-ext,vietnamese" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900iSource+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>

		<script src="js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen">
		<!--light-box-files -->
		<script>
		$(function() {
			$('.gallery-grid a').Chocolat();
		});
		</script>

<!-- //js -->
<script src="js/responsiveslides.min.js"></script>
		<script>
				$(function () {
					$("#slider").responsiveSlides({
						auto: true,
						pager:false,
						nav: true,
						speed: 1000,
						namespace: "callbacks",
						before: function () {
							$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
						}
					});
				});
			</script>
			

<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<style>
	.twin-button{
		padding:10px 20px;
		width:20%;
		font-size:18px;
		outline:0;
		border:0;
		box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);
		border-radius: 0;
	}
	.twin-button:focus{
		outline:0 !important;
	}
</style>


</head>
<body>
<div class="header" id="home">
	<div class="logo">
		<a href="#"><h1>SHS_Classifier</h1></a>
	</div>
<!-- navigation -->
		<div class="ban-top-con">
			<div class="top_nav_left">
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav menu__list">
						<li class="active menu__item menu__item--current"><a class="menu__link " href="#home">Home <span class="sr-only">(current)</span></a></li>
						<li class=" menu__item"><a class="menu__link scroll" href="#about">About us</a></li>
						<li class=" menu__item"><a class="menu__link scroll" href="#management">Management</a></li>
						<li class=" menu__item"><a class="menu__link scroll" href="#activities">Activities</a></li>
						<li class=" menu__item"><a class="menu__link scroll" href="#faculties">Faculties</a></li>
						<li class=" menu__item"><a class="menu__link scroll" href="#contact">contact</a></li>
						<li class=" menu__item"><a class="menu__link" href="<?php echo $app_url; ?>/register-student">Enroll Now</a></li>
						<li class=" menu__item"><a class="menu__link" href="<?php echo $app_url; ?>/login">Admin</a></li>
					  </ul>
					</div>
				  </div>
				</nav>	
				
			</div>
			<div class="clearfix"></div>
			</div>
	<!-- //navigation -->
<!-- Slider -->
		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides" id="slider">
					<li>
						<div class="slider-img">
							<img src="images/bg1.jpg" class="img-responsive" alt="education">
						</div>
						<div class="slider-info">
							<h3>SHS_Classifier</h3>
							<p>An Integrated Student Strand Classifier for Senior High School Pre Enrollment System</p>
							<div class="custom-btns">
								<a href="http://localhost:4200/register-student" type="button" class="btn btn-danger twin-button">Enroll Now</a>
								<a href="http://localhost:4200/fillup" type="button" class="btn btn-danger twin-button">View Record</a>
							</div>
						</div>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- //Slider -->
</div>
<!--main-content-->
<div class="agile-main" id="about">
	<div class="container">
	<!--about-->
		<div class="about">
			<h2>about us</h2>
			<h4>Senior high school (SHS) refers to Grade 11 and 12, the last two years of the K-12 program that DepEd has been implementing since 2012. Students begin to study in SHS the subjects that will introduce them to their preferred career path. There are different type of tracks and strand to choose such as STEM, HUMSS, GAS, EIM, CSS, TOURISM and many more.</h4>
			<img src="images/su.jpg" alt="sucess">
			<p>As today’s reception to the curriculum in accordance with the K-12 program implementation, many factors are affecting the senior high school student’s strand preference. The decision on good career choice and school sometimes depend on how the way students perceive the world and their future. Many junior High school students have difficulty in choosing the right strand.</p>		
			<p>The proposed project will replace the manual process of the school’s existing manual enrollment system and classifying student strand, also use computer based software that enables authorized users to speed up the enrollment process for Senior High school, provide an updated, secured and easy to access student records, and generate accurate reports.</p>
		</div>
		<div class="clearfix"></div>
	<!--//about-->
	</div>
</div>
<!--meet our management-->
<div class="team" id="management">
	<div class="container">
		<h3>meet our management</h3>
		<p>Behind the successs of this Capstone Projects are the following people</p>
		<div class="w3grids">
			<div class="w3grid col-md-3">
				<img src="images/t1_2.jpg" alt="team1" class="img1-w3l">
				<h5>John Paul</h5>
				<p>Project Manager | Tester</p>
				<p>johnpauldulog69@gmail.com</p>
				<p>09676174621</p>
				<div class="socialw3-icons">
					<i class=" so1 fa fa-facebook" aria-hidden="true"></i>
					<i class=" so2 fa fa-twitter" aria-hidden="true"></i>
					<i class=" so3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
			<div class="w3grid col-md-3">
				<img src="images/t2_2b.jpg" alt="team1" class="img2-w3l">
				<h5>Jhoanna Marrie</h5>
				<p>Technical Writer</p>
				<p>jhoannamarrierpastera@gmail.com</p>
				<p>09261746982</p>
				<div class="socialw3-icons">
					<i class=" so1 fa fa-facebook" aria-hidden="true"></i>
					<i class=" so2 fa fa-twitter" aria-hidden="true"></i>
					<i class=" so3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
			<div class="w3grid col-md-3">
				<img src="images/t3.jpg" alt="team1" class="img3-w3l">
				<h5>Lesther</h5>
				<p>Programmer | System Analyst</p>
				<p>lestherbualan1718@gmail.com</p>
				<p>09973296848</p>
				<div class="socialw3-icons">
					<i class=" so1 fa fa-facebook" aria-hidden="true"></i>
					<i class=" so2 fa fa-twitter" aria-hidden="true"></i>
					<i class=" so3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
			<div class="w3grid col-md-3">
				<img src="images/t4_2.jpg" alt="team1" class="img4-w3l">
				<h5>Francis Rey</h5>
				<p>Group Adviser | Mentor</p>
				<p>francisreypadao@gmail.com</p>
				<p>09776920392</p>				
				<div class="socialw3-icons">
					<i class=" so1 fa fa-facebook" aria-hidden="true"></i>
					<i class=" so2 fa fa-twitter" aria-hidden="true"></i>
					<i class=" so3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!--//meet our management-->

<!--welcome-->
<div class="w3l-welcome">
	<div class="container">
		<div class=" agile-welcome">
			<div class="text-w3">
				<h4>welcome to our university</h4>
				<p>Name of School here</p>
			</div>
			<div class="grids">
				<div class="grid">
					<div class="icons">
						<i class="fa fa-book" aria-hidden="true"></i>
					</div>
					<div class="text">
						<h5>SKILLED PROFESSORS</h5>
						<p>The school hired skilled professors for good teaching.</p>
					</div>
				</div>
				<div class="grid">
					<div class="icons">
						<i class="fa fa-thumbs-up" aria-hidden="true"></i>
					</div>
					<div class="text">
						<h5>Career Growth</h5>
						<p>The school gives you a suitable course for your better future.</p>
					</div>
				</div>
				<div class="grid">
					<div class="icons">
						<i class="fa fa-table" aria-hidden="true"></i>
					</div>
					<div class="text">
						<h5>BIG LIBRARY</h5>
						<p>School has library for all you need to know in your research and studies.</p>
					</div>
				</div>
				
				<div class="grid">
					<div class="icons">
						<i class="fa fa-laptop" aria-hidden="true"></i>
					</div>
					<div class="text">
						<h5>WELL EQUIPPED LABS</h5>
						<p>The school laboratories are equipped with all the aparatus you need.</p>
					</div>
				</div>
			</div>
			<div class="w3-img">
				<img src="images/man2.jpg" alt="image" />
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--//welcome-->

<!--activities-->
<div class="gallery" id="activities">
	<div class="container">
	  <div class="gallery-main">
	  	<div class="gallery-top">
	  		<h3>our activities</h3>
	  	</div>
		<div class="gallery-bott">
			<div class="col-md-4 col1 gallery-grid">
				<a href="images/g1.jpg" class="b-link-stripe b-animate-go  thickbox">

						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g1.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
					</a>
					</div>
					<!--
					<div class="col-md-4 col1 gallery-grid">
						<a href="images/g2.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g2.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
						</a>
					</div>
					-->
					<div class="col-md-4 col1 gallery-grid">
						<a href="images/g3.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g3.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
						</a>
					</div>
					
			     <div class="col-md-4 col1 gallery-grid">
				  <a href="images/g4.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g4.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
					</a>
					</div>

					<div class="col-md-4 col1 gallery-grid">
						<a href="images/g5.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g5.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
						</a>
					</div>
					<div class="col-md-4 col1 gallery-grid">
						<a href="images/g6.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g6.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
						</a>
					</div>
					<!--
					<div class="col-md-4 col1 gallery-grid">
						<a href="images/g7.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g7.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
						</a>
					</div>

					<div class="col-md-4 col1 gallery-grid">
						<a href="images/g8.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g8.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
						</a>
					</div>
-->
					<div class="col-md-4 col1 gallery-grid">
						<a href="images/g9.jpg" class="b-link-stripe b-animate-go  thickbox">
						<figure class="effect-bubba">
							<img class="img-responsive" src="images/g9.jpg" alt="">
							<figcaption>
								<h4 class="gal">Education Hub</h4>
								<p class="gal1">“Live as if you were to die tomorrow. Learn as if you were to live forever.” </p>	
							</figcaption>			
						</figure>
						</a>
					</div>
			     <div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
<!--//activities-->



	<!-- Custom-JavaScript-File-Links -->
	<!-- Countdown-Timer-JavaScript -->
			<script src="js/simplyCountdown.js"></script>
			<script>
				var d = new Date(new Date().getTime() + 48 * 120 * 120 * 2000);

				// default example
				simplyCountdown('.simply-countdown-one', {
					year: d.getFullYear(),
					month: d.getMonth() + 1,
					day: d.getDate()
				});

				// inline example
				simplyCountdown('.simply-countdown-inline', {
					year: d.getFullYear(),
					month: d.getMonth() + 1,
					day: d.getDate(),
					inline: true
				});

				//jQuery example
				$('#simply-countdown-losange').simplyCountdown({
					year: d.getFullYear(),
					month: d.getMonth() + 1,
					day: d.getDate(),
					enableUtc: false
				});
			</script>
			
		<!-- //Countdown-Timer-JavaScript -->
	<!-- //Custom-JavaScript-File-Links -->
</div>	
<!--// opening -->

<!--faculty-->
<div class="w3-faculty" id="faculties">
	<div class="container">
		<div class="faculty-head">
			<h5>our great faculties</h5>
			<p>“Books are the quietest and most constant of friends; they are the most accessible and wisest of counselors, and the most patient of teachers.” </p>
		</div>
		<div class="main-faculty">
			<div class="f1 col-md-3 faculty1">
				<ul class="demo-2 effect">
					<li>
					   <h3 class="zero">GENERAL SCIENCE</h3>
					</li>
					 <li><img class="top" src="images/t1_2.jpg" alt=""/></li>
				</ul>
				<h4>John Paul</h4>
				<div class="social-icons">
					<i class="s1 fa fa-facebook" aria-hidden="true"></i>
					<i class="s2 fa fa-twitter" aria-hidden="true"></i>
					<i class="s3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
			<div class="f2 col-md-3 faculty1">
				<ul class="demo-2 effect">
					<li>
					   <h3 class="zero">COMMUNICATION SKILLS</h3>
					</li>
					 <li><img class="top" src="images/t2_2b.jpg" alt=""/></li>
				</ul>
				<h4>Jhoanna Marrie</h4>
				<div class="social-icons">
					<i class=" s1 fa fa-facebook" aria-hidden="true"></i>
					<i class=" s2 fa fa-twitter" aria-hidden="true"></i>
					<i class=" s3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
			<div class="f3 col-md-3 faculty1">
				<ul class="demo-2 effect">
					<li>
					   <h3 class="zero">COMPUTER SCIENCE</h3>
					</li>
					 <li><img class="top" src="images/t3.jpg" alt=""/></li>
				</ul>
				<h4>Lesther</h4>
				<div class="social-icons">
					<i class=" s1 fa fa-facebook" aria-hidden="true"></i>
					<i class=" s2 fa fa-twitter" aria-hidden="true"></i>
					<i class=" s3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
			<div class="f4 col-md-3 faculty1">
				<ul class="demo-2 effect">
					<li>
					   <h3 class="zero">MATHEMATICS</h3>
					</li>
					 <li><img class="top" src="images/t4_2.jpg" alt=""/></li>
				</ul>
				<h4>Francis Rey</h4>
				<div class="social-icons">
					<i class="s1 fa fa-facebook" aria-hidden="true"></i>
					<i class="s2 fa fa-twitter" aria-hidden="true"></i>
					<i class="s3 fa fa-google" aria-hidden="true"></i>
				</div>
			</div>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!--//faculty-->

<!--contact-->
<div class="agile-contact" id="contact">
	<div class="left-contact">

			<h6>contact us</h6>
			<ul>
				<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@example.com</a></li>
				<li><i class="fa fa-phone" aria-hidden="true"></i>+63123456890</li>
				<li><i class="fa fa-map-marker" aria-hidden="true"></i>Southern Leyte State University</li>
			</ul>
	
	</div>
	<div class="right-contact">

	</div>
	<div class="clearfix"></div>
</div>
<!--//contact-->
<!--//main-content-->

<!--footer-->
<div class="w3l-footer">
	<div class="container">
		<div class="left-w3">
			<a href="#">SHS_Classifier</a>
		</div>
		<div class="right-social">
			<i class="fa fa-facebook-square" aria-hidden="true"></i>
			<i class="fa fa-twitter-square" aria-hidden="true"></i>
			<i class="fa fa-google-plus-square" aria-hidden="true"></i>
		</div>
		<div class="clearfix"></div>
		<div class="footer-nav">
			<ul>
				<li><a class="menu__link scroll" href="#home">home</a></li>
				<li><a class="menu__link scroll" href="#about">about</a></li>
				<li><a class="menu__link scroll" href="#management">management</a></li>
				<li><a class="menu__link scroll" href="#activities">activities</a></li>
				<li><a class="menu__link scroll" href="#faculties">faculties</a></li>
				<li><a class="menu__link scroll" href="#contact">contact</a></li>
			</ul>
		</div>
		<div class="copyright-agile">
			<p>&copy; 2016 SLSU Main Campus. All rights reserved</p>
		</div>
	</div>
</div>
<!--//footer-->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
</body>
</html>
