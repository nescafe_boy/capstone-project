import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { BehaviorSubject } from 'rxjs';
import { UserModel } from '../model';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {

  private _user = new BehaviorSubject<UserModel>(null);

  constructor(private _httpService: HttpService) {}

  getUser() {
    return this._user.asObservable();
  }

  setUser(user: UserModel) {
    return this._user.next(user);
  }

  fetch() {
    return this._httpService.getRequest('/api/user');
  }
}
