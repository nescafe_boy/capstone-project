import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, Form } from '@angular/forms';
import { ClassifierService } from 'src/app/services/classifier.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  form:FormGroup;

  constructor(private _fb: FormBuilder , private _classify: ClassifierService) { }

  ngOnInit() {
    this.form = this._fb.group({
      gender: ['',[Validators.required]],
      ogsa_sc: ['',[Validators.required]],
      ogsa_pr: ['',[Validators.required]],
      otva_sc: ['',[Validators.required]],
      otva_pr: ['',[Validators.required]],
      oat_sc: ['',[Validators.required]],
      oat_pr: ['',[Validators.required]],
      average: ['',[Validators.required]]
    });
  }

  submit(){
    const form = {...this.form.value};
    //console.log(form);
   // this._classify.pass(form).toPromise().then((responce)=>{
     // console.log(responce);
   // });
  }

}
