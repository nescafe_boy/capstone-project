import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ViewStudentInfoService {
  id :any;
  constructor() { }
   setID(id){
        this.id = id;
      }
   getID(){
      return localStorage.getItem('id');
  }
}
