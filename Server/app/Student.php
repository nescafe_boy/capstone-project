<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'lrn_input', 
        'studlname_input', 
        'studfname_input', 
        'studmname_input', 
        'studdate_input', 
        'studgender_input', 
        'studcity_input', 
        'studzip_input', 
        'pfatherfn_input', 
        'pfathermn_input', 
        'pfatherln_input', 
        'pmotherfn_input', 
        'pmothermn_input', 
        'pmotherln_input', 
        'contact',
        'ogsa_sc',
        'ogsa_pr',
        'otva_sc',
        'otva_pr',
        'oat_sc',
        'oat_pr',
        'average',
        'strand',
        'ref_code'
    ];
}
