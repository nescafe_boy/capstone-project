import { Component, OnInit } from '@angular/core';
import { EnrollStudentModel } from 'src/app/model';
import { StudentconfirmService } from 'src/app/services/studentconfirm.service';
import { EnrolledstudentService }from 'src/app/services/enrolledstudent.service';
import { EnrollstudentService } from 'src/app/services/enrollstudent.service';
@Component({
  selector: 'app-enrolled-student',
  templateUrl: './enrolled-student.component.html',
  styleUrls: ['./enrolled-student.component.css']
})
export class EnrolledStudentComponent implements OnInit {
  enrolledStudents: EnrollStudentModel[];
  sortedData: EnrollStudentModel[];
  setDeleteID:any;

  constructor(
    private _StudentConfirm : StudentconfirmService,
    private _EnrolledstudentService : EnrolledstudentService,
    private _EnrollstudentService:EnrollstudentService
  ) { }

  ngOnInit() {
   
    this._EnrolledstudentService.fetch().toPromise().then((enrolledStudents:EnrollStudentModel[])=>{
      this._EnrolledstudentService.setStudent = enrolledStudents;
    });

    this._EnrolledstudentService.getStudent.subscribe((enrolledStudents:EnrollStudentModel[])=>{
      this.enrolledStudents = enrolledStudents;
    });

  }

  private discard(id){
    this.setDeleteID = id;
  }

  private deleteStudent(){
    
    this._EnrollstudentService.destroy(this.setDeleteID).toPromise().then((resp)=>{
      console.log(resp);
      this.ngOnInit();
    }).catch((error)=>{
      console.log(error);
    });
    
  }

  private sort(data:any){
    this._EnrolledstudentService.sortResult(data.target.value).toPromise().then((sortedData: EnrollStudentModel[])=>{
      this.enrolledStudents = sortedData;
    });  
  }

}
