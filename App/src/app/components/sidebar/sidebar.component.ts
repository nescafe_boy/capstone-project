import { Component, OnInit, OnDestroy } from '@angular/core';
import { CurrentUserService } from 'src/app/services/current-user.service';
import { UserModel } from 'src/app/model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

  currentUser: UserModel;

  private _currentUserSubscription: Subscription;

  constructor(private _currentUserService: CurrentUserService) { }

  ngOnInit() {
    this._currentUserSubscription = this._currentUserService.getUser().subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnDestroy() {
    this._currentUserSubscription.unsubscribe();
  }

}
