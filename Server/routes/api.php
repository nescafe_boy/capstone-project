<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function() {
    Route::resource('register', 'RegisterStudentController');
    Route::resource('classify', 'ClassifierController');
    Route::resource('enrollStudent', 'EnrollStudentController');

    Route::group(['middleware' => ['api', 'auth:api']], function () {
        Route::resource('getStudent', 'StudentsController');
        Route::resource('registeradmin', 'RegisterAdminController');

    });

});
