import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { EnrollStudentModel } from '../model';

@Injectable({
  providedIn: 'root'
})
export class EnrollstudentService {

  constructor(private _http:HttpService) { }

  create(enrollStudent: EnrollStudentModel){
    return this._http.postRequest('/api/enrollStudent', enrollStudent);
  }
  destroy(id:any){
    return this._http.deleteRequest('/api/enrollStudent/'+id);
  }
}
