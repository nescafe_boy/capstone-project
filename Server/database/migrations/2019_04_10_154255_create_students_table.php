<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lrn_input');
            $table->string('studlname_input');
            $table->string('studfname_input');
            $table->string('studmname_input');
            $table->string('studdate_input');
            $table->string('studgender_input');
            $table->string('studcity_input');
            $table->string('studzip_input');
            $table->string('pfatherfn_input');
            $table->string('pfathermn_input');
            $table->string('pfatherln_input');
            $table->string('pmotherfn_input');
            $table->string('pmothermn_input');
            $table->string('pmotherln_input');
            $table->string('contact');
            $table->string('ogsa_sc');
            $table->string('ogsa_pr');
            $table->string('otva_sc');
            $table->string('otva_pr');
            $table->string('oat_sc');
            $table->string('oat_pr');
            $table->string('average');
            $table->string('strand');
            $table->string('ref_code');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
