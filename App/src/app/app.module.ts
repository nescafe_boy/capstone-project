import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnrollmentPageComponent } from './components/enrollment-page/enrollment-page.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { ClassifyComponent } from './components/classify/classify.component';
import { AddTeacherComponent } from './components/add-teacher/add-teacher.component';
import { AuthComponent } from './components/auth/auth.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { GuessNavComponent } from './components/guess-nav/guess-nav.component';
import { StudentlistComponent } from './components/studentlist/studentlist.component';
import { ModalComponent } from './components/modal/modal.component';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { EnrolledStudentComponent } from './components/enrolled-student/enrolled-student.component';
import { CreateAdminAccountComponent } from './components/create-admin-account/create-admin-account.component';
import { ViewStudentsInfoComponent } from './components/view-students-info/view-students-info.component';
import { FillupFormComponent } from './components/fillup-form/fillup-form.component';

@NgModule({
  declarations: [
    AppComponent,

    EnrollmentPageComponent,
    LoginFormComponent,
    ClassifyComponent,
    AddTeacherComponent,
    AuthComponent,
    SidebarComponent,
    NavbarComponent,
    GuessNavComponent,
    StudentlistComponent,
    ModalComponent,
    AdminHomeComponent,
    EnrolledStudentComponent,
    CreateAdminAccountComponent,
    ViewStudentsInfoComponent,
    FillupFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
