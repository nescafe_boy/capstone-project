<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\EnrolledStudent;

class EnrollStudentController extends Controller
{
    public function store(Request $request){
        return EnrolledStudent::create([
            'lrn_input'         => $request->lrn_input,
            'studlname_input'   => $request->studlname_input,
            'studfname_input'   => $request->studfname_input,
            'studmname_input'   => $request->studmname_input,
            'studdate_input'    => $request->studdate_input,
            'studgender_input'  => $request->studgender_input,
            'studcity_input'    => $request->studcity_input,
            'studzip_input'     => $request->studzip_input,
            'pfatherfn_input'   => $request->pfatherfn_input,
            'pfathermn_input'   => $request->pfathermn_input,
            'pfatherln_input'   => $request->pfatherln_input,
            'pmotherfn_input'   => $request->pmotherfn_input,
            'pmothermn_input'   => $request->pmothermn_input,
            'pmotherln_input'   => $request->pmotherln_input,
            'contact'           => $request->contact,
            'ogsa_sc'           => $request->ogsa_sc,
            'ogsa_pr'           => $request->ogsa_pr,
            'otva_sc'           => $request->otva_sc,
            'otva_pr'           => $request->otva_pr,
            'oat_sc'            => $request->oat_sc,
            'oat_pr'            => $request->oat_pr,
            'average'           => $request->average,
            'strand'            => $request->strand,
            'ref_code'          => uniqid()
        ]);
    }
    public function index(Request $request){
        return EnrolledStudent::get();
    }
    public function destroy($id){
        $status = EnrolledStudent::find($id)->delete();
        return [$status];
    }
    public function show($data){
        return EnrolledStudent::orderBy($data,'ASC')->get();
    }
}
