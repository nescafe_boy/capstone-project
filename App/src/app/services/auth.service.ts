import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LoginModel, AccessTokenModel } from '../model';
import { environment } from 'src/environments/environment';
import { HttpService } from './http.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

    private _authRedirectTo: string = '/auth';
    private _logoutRedirect: string = 'http://localhost/thesis/landing-page';

    constructor(private _http: HttpClient, private _router: Router) { }

   login(loginData: LoginModel) {
        const data = environment.serverPassportConfig;
        data.username = loginData.username;
        data.password = loginData.password;
        //console.log(data);
        this._generateAccessToken(data).toPromise().then((accessToken: AccessTokenModel) => {
            console.log(accessToken);
            this._setAccessToken = accessToken;
            this._router.navigate([this._authRedirectTo]);
        });
    }

    logout() {
        localStorage.removeItem('accessToken');
        window.location.href = this._logoutRedirect;
    }

  	get auth() {
    	return JSON.parse(localStorage.getItem('accessToken'));
	}

    private _generateAccessToken(loginData: LoginModel) {
        return this._http.post(environment.serverUrl + '/oauth/token', loginData);
    }

    private set _setAccessToken(accessToken: AccessTokenModel) {
        localStorage.setItem('accessToken', JSON.stringify(accessToken));
    }
}
