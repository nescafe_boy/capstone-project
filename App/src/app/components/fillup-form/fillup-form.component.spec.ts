import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillupFormComponent } from './fillup-form.component';

describe('FillupFormComponent', () => {
  let component: FillupFormComponent;
  let fixture: ComponentFixture<FillupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillupFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
