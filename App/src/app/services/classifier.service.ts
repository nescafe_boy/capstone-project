import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { ClassifyData } from '../model';

@Injectable({
  providedIn: 'root'
})
export class ClassifierService {

  constructor(private _http: HttpService) { }
  
  pass(data: ClassifyData){
    return this._http.postRequest('/api/classify', data);
    //return console.log(data);
  }

}
